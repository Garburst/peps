<?php

	/*--------------------------------------------------------------------------------------*\
	
	On stock nos différentes fonctions qu'on utilise dans la page 'traitementFormulaire.php'
	
	\*--------------------------------------------------------------------------------------*/
	

	//Fonction pour ajouter (upload) un fichier sur le serveur avec en paramètre le nom du fichier qu'on upload, la valeur de la balise 'name' du formulaire pour le fichier
	function traitementUploadFichier ($nomFichier){
		//Permet de nous servir de nos tableaux et de les modifier à l'intérieur d'une fonction(ref : Upload des fichiers sur le serveur)
		global $nomDossierUt, $stockInfoFichierServeur, $extensionsAutorisees;

		//Récupère le nom du fichier rentré en paramètre grâce à la variable super globale $_FILES
		$nomOrigine = $_FILES[$nomFichier]['name'];
		//Récupère les informations sur le fichier dans un tableau - Récupère l'extension du fichier
		$elementsChemin = pathinfo($nomOrigine);
		$extensionFichier = $elementsChemin['extension'];
		//Récupère le nom du fichier utilisateur - Remplace les espaces et les points par des '_'
		$nomFichierUt = $elementsChemin['filename'];
		$nomFichierUt = str_replace(' ','_',$nomFichierUt);
		$nomFichierUt = str_replace('.','_',$nomFichierUt);

		//Initialisation de 'bonneExtension' à false, on la passe à true seulement si on trouve la bonne extension						
		$bonneExtension = false;
		for($j = 0; $j < sizeof($extensionsAutorisees[$nomFichier]);$j++){
			//Parcourt le tableau 'extensionsAutorisees' à la recherche de l'extension 'extensionFichier' en fonction du fichier 'nomFichier'
			if($extensionsAutorisees[$nomFichier][$j] == $extensionFichier){
				$bonneExtension = true;
			}
		}
		
		if (!($bonneExtension)) {
			echo "Le fichier ".$nomFichier." n'a pas l'extension attendue. <br>";
		} else {    
			//Remplace les '\' par des '/' pour A REVOIR et enlève 'include/' du chemin
			$repertoireDestination = str_replace("\\","/",dirname(__FILE__)."/");
			//Construit le chemin du fichier, ex : dossierUt/20190605144430/photoAuteur.png
			$nomDestinationFichier = "dossierUt/".$nomDossierUt."/".$nomFichierUt.".".$extensionFichier;	
			//Si le déplacement du fichier téléchargé ce passe bien, alors on stock son chemin, son extension, son nom et 'TRUE' à l'attribut upload
			if (move_uploaded_file($_FILES[$nomFichier]["tmp_name"], $repertoireDestination.$nomDestinationFichier)) {
				$stockInfoFichierServeur[$nomFichier] = array('chemin'=> $nomDestinationFichier, 'extension'=> $extensionFichier, 'nom'=> $nomFichierUt, 'upload'=> TRUE);
			} else {
				echo "Le fichier '".$nomFichier."' n'a pas été uploadé. <br>";
			}
		}
	}

	//Fonction pour copier un dossier qui prend en argument le chemin du dossier à copier et le chemin de la destination
	function copierDossier ($dossierACopier,$dossierDestination) {
		//On vérifie si $dossierACopier est un dossier
		if (is_dir($dossierACopier)) {
			//Si oui, on l'ouvre
			if ($dossierOuvert = opendir($dossierACopier)) {     
				//On liste les dossiers et fichiers de $dossierACopier
				while (($file = readdir($dossierOuvert)) !== false) {
					//Si le dossier dans lequel on veut coller n'existe pas, on le créé
					if (!is_dir($dossierDestination)) mkdir ($dossierDestination);
		 
					//S'il s'agit d'un dossier, on relance la fonction récursive sinon s'il s’agit d'un fichier, on le copie simplement
					if(is_dir($dossierACopier.$file) && $file != '..'  && $file != '.'){
						
						copierDossier ( $dossierACopier.$file.'/' , $dossierDestination.$file.'/' );   
						
					} elseif($file != '..'  && $file != '.'){
						copy ( $dossierACopier.$file , $dossierDestination.$file );  
					}
				}
			  //On ferme $dossierACopier
			  closedir($dossierOuvert);
			}
		}
	}
	
	//Fonction pour supprimer un dossier qui prend en argument le chemin du dossier
	function supprimerDossier($dossier) {
		//On vérifie si $dossier est un dossier
		if (is_dir($dossier)) {
			//On scan le dossier pour récupérer ses objets : dossiers et fichiers - Pour chaque objet on fait
			$objets = scandir($dossier);
			foreach ($objets as $unObjet) {
				//Si l'objet n'est pas . ou .. , si l'objet est un dossier on appelle la fonction récursive sinon c'est un fichier et on le supprime
				if ($unObjet != "." && $unObjet != "..") {
					if (filetype($dossier."/".$unObjet) == "dir"){
						supprimerDossier($dossier."/".$unObjet);
					} else {
						unlink($dossier."/".$unObjet);
					}
				}
			}	
			//Place le pointeur au premier élément et on supprime le dossier
			reset($objets);
			rmdir($dossier);
		}
	}
			

	//Recherche le fichier passé en paramètre dans l'Exodus Utilisateur
	function trouverFichier($typeFichier){
		//Permet d'utiliser nos deux variables à l'intérieur d'une fonction
		global $nomDossierUt, $nomExodus;
		
		if($typeFichier == 'fichierAudio'){
			//Analyse le dossier 'sons' de l'Exodus de l'Utilisateur
			$dossier = scandir('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/sons');
			//Pour chaque fichier on scinde son nom et son extension pour regrouper ces informations dans un tableau ([0] = nomFichier, [1] = extension fichier)
			foreach ($dossier as $fichier){
				$decoupeFichier = explode('.', $fichier);
				//Si ce n'est pas REVOIR , comme le dossier contient qu'un seul fichier, qu'on ne connaît pas son nom, on cherche un fichier qui a une extension acceptable
				if($fichier != '.' and $fichier != '..' and ($decoupeFichier[1] == 'mp3' or $decoupeFichier[1] == 'ogg' or $decoupeFichier[1] == 'aac')){
					//Retourne les informations du ficher trouvé sous la forme d'un tableau associatif
					$chemin = 'dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/sons/'.$fichier;
					return array('chemin'=> $chemin,'nom'=>$decoupeFichier[0],'ext'=>$decoupeFichier[1],'nomComplet'=>$fichier);
				}
			}
		}
		
		if($typeFichier == 'fichierImage'){
			//Analyse le dossier 'images' de l'Exodus de l'Utilisateur
			$dossier = scandir('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/images');
			//Pour chaque fichier on scinde son nom et son extension pour regrouper ces informations dans un tableau ([0] = nomFichier, [1] = extension fichier)
			foreach ($dossier as $fichier){	
				$decoupeFichier = explode('.', $fichier);						
				//Si ce n'est pas REVOIR , comme le dossier contient qu'un seul fichier, qu'on ne connaît pas son nom, on cherche un fichier qui a une extension acceptable
				if($fichier != '.' and $fichier != '..' and $decoupeFichier[0] != 'logo' and ($decoupeFichier[1] == 'jpg' or $decoupeFichier[1] == 'png' or $decoupeFichier[1] == 'jpeg')){
					//Retourne les informations du ficher trouvé sous la forme d'un tableau associatif
					$chemin = 'dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/images/'.$fichier;
					return array('chemin'=> $chemin,'nom'=>$decoupeFichier[0],'ext'=>$decoupeFichier[1],'nomComplet'=>$fichier);
				}
			}
		}
	}

?>