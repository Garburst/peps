
<?php

	/*--------------------------------------------------------------------------------------*\
	
				Lit le fichier 'dataExodus' pour remplir les champs du formulaire
	
	\*--------------------------------------------------------------------------------------*/
	

	//Ici, on code dans les balises 'script' du fichier 'correction.php', c'est pour cela qu'on écrit du jquery dans des echo
	
	$fichierDataExodus = fopen("../dossierUt/".$nomDossierUt."/".$nomExodus."/edit/dataExodus.txt", "r");


	////------------------------ Informations ------------------------\\\\

	//Lit la ligne suivante du fichier et scinde la chaîne de caractères en segments dans un tableau, ce qui donne : [1]=>'titreConference', [2]=>'leTitre'
	//Le séparateur étant "|" (titreConference|leTitre)
	$ligneActuelle = explode('|',fgets($fichierDataExodus));
	//Tant qu'on a pas finit la 1er Partie du fichier data, on remplit les champs du formulaire
	while($ligneActuelle[0] != ""){
		//Récupère le nombre de diapo dans une variable
		if($ligneActuelle[0] == "nombreDeDiapo"){
			$nombreDeDiapo = $ligneActuelle[1];
		}
		//Attribut une valeur pour le nom de l'input. ex : $('#titreConference').val("Iceberg");
		echo "					$('#".$ligneActuelle[0]."').val(\"".$ligneActuelle[1]."\");\n";
		$ligneActuelle = explode('|',fgets($fichierDataExodus));
	}

	//Ecrit un commentaire et appelle la fonction 'setNombreDiapo'
	echo "
					//initialisation de la liste de diapo pour la partie 1, elle est toujours présente mais jamais initialisé au nombre de diapo
					//par rapport aux autres parties où le 'setNombreDiapo' est fait quand on ajoute une partie
					setNombreDiapo('#diapoPartie1');\n";
	
	
	////------------------------ TDM ------------------------\\\\
	
	//Passe à la ligne suivante		
	$ligneActuelle = explode('|',fgets($fichierDataExodus));
	//Tant qu'on a pas finit la 2ème Partie du fichier data, on construit la table des matières -
	//Initialisation de la variable 'compteurPartie' à 1 qui permet de savoir à quelle partie on est
	for($compteurPartie = 1; $ligneActuelle[0] != ""; $compteurPartie++){
		
		//Si c'est une 'Partie' alors
		if($ligneActuelle[0] == 'Partie'){
			//Comme il y a toujours au moins 1 Partie dans la TDM, on ajoute une nouvelle partie seulement si on a déjà remplit une Partie, donc compteurPartie supérieure à 1
			if($compteurPartie > 1){
				//Appelle la fonction 'ajouterUnePartie'
				echo "					ajouterUnePartie();\n";
			}
			//Remplit les champs de la Partie, son nom ainsi que sa diapo liée - Passe à la ligne suivante
			echo "					$('#nomPartie".$compteurPartie."').val(\"".$ligneActuelle[1]."\");
					$('#diapoPartie".$compteurPartie."').val('".$ligneActuelle[2]."');\n";
			$ligneActuelle = explode('|',fgets($fichierDataExodus));
		}

		//Si la ligne suivante est une sous partie, tant que c'est le cas alors
		for($i = 1; $ligneActuelle[0] == 'SousPartie'; $i++){
			//Appelle la fonction 'ajouterUneSousPartie' 
			//Remplit les champs de la SousPartie, son nom ainsi que sa diapo liée
			//'compteurPartie' permet de savoir à quelle partie X il faut ajouter la SousPartie numéro 'i' donc Y (SousPartieX_Y)
			echo "					ajouterUneSousPartie('conteneur".$compteurPartie."');
					$('#nomSousPartie".$compteurPartie."_".$i."').val(\"".$ligneActuelle[1]."\");
					$('#diapoSousPartie".$compteurPartie."_".$i."').val('".$ligneActuelle[2]."');\n";
			//Passe à la ligne suivante
			$ligneActuelle = explode('|',fgets($fichierDataExodus));
		}
		//Une fois que la Partie a été traité, et que ses SousParties potentielles ont été traitées, on boucle jusqu'à arriver à la fin de la TDM
	}
	
	echo "					//met à jour le nombre de partie
					$('#nombreDePartie').val($('.niveau1').length); \n\n";
	
	
	////------------------------ Marqueurs ------------------------\\\\

	$ligneActuelle = explode('|',fgets($fichierDataExodus));
	for($compteurMarqueur = 1; $ligneActuelle[0] != ""; $compteurMarqueur++){
		$marqueurConvertit = gmdate("H:i:s", $ligneActuelle[1]);
		//Comme il y a toujours au moins 1 Marqueur, on ajoute un nouveau marqueur seulement si on a déjà remplit un marqueur, donc compteurMarqueur supérieure à 1
		if($compteurMarqueur > 1){
			echo "					ajouterUnMarqueur('".$marqueurConvertit."');\n";
		} else {
			//Sinon on appelle la fonction 'modifierMarqueur' pour rentrer la valeur du 1er marqueur
			echo "					modifierMarqueur('1', '00:00:00');\n";
		}

		$ligneActuelle = explode('|',fgets($fichierDataExodus));
	}
	
	//A la dernière itération, le compteur prend +1 en 'trop', on le décrémente pour avoir le nombre exacte de marqueur
	$compteurMarqueur --;
	//On ajuste le nombre de diapo en fonction du nombre de marqueur
	echo "\n					$('#nombreDeDiapo').val('".$compteurMarqueur."');\n";

	//Pour la mise en page du code on saute une ligne
	echo "\n";
	
	fclose($fichierDataExodus);
	
?>
