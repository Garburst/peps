<?php

	/*--------------------------------------------------------------------------------------*\
	
								Construit le fichier texte 'dataExodus'
	
	\*--------------------------------------------------------------------------------------*/
	

	//Si on ne modifie pas l'Exodus
	if (!(isset($_POST['modifData']))){
		//Initialisation de la chaîne 'blocTDM'
		$blocTDMData = ""; // TDM => Table Des Matières
		//Tant qu'on arrive pas au nombre de partie, on ajoute une ligne à la liste de la TDM
		//soit il n'y a pas de sous partie et on ne rentre pas dans le if et on ferme la ligne directement
		//soit il y a une sous partie et on ajoute autant de fois qu'il le faut de sous partie
		for($posPartie = 1; $posPartie <= $nombreDePartie; $posPartie++){ //posPartie => positionPartie

			$blocTDMData .= "Partie|".$_POST['nomPartie'.$posPartie]."|".$_POST['diapoPartie'.$posPartie].'|'.PHP_EOL;

			//Si il y a au moins une sous partie à la partieX, alors 'nomSousPartieX_1' est déclaré, et donc si c'est bien le cas on fait
			if (isset($_POST['nomSousPartie'.$posPartie.'_1'])){	
				//Tant qu'il y a 'nomSousPartieX_Y' qui est déclaré, alors on écrit la sous partie dans le fichier ('SousPartie|La fonte des glaces|diapo005')
				for($posSousPartie = 1; isset($_POST['nomSousPartie'.$posPartie.'_'.$posSousPartie]); $posSousPartie++){
					
					$blocTDMData .= "SousPartie|".$_POST['nomSousPartie'.$posPartie.'_'.$posSousPartie]."|".$_POST['diapoSousPartie'.$posPartie.'_'.$posSousPartie]."|".PHP_EOL;

				}
			}
		}
	}

	//Écrit le fichier 'data.txt' en 3 Parties en les séparant par un saut de ligne et un '|'
	//1) Les informations du formulaire
	//2) Le bloc de la table des matières crée juste au-dessus
	//3) La première colonne du fichier des marqueurs
	$fichierDataExodusTxt = fopen('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/edit/dataExodus.txt','w');
	fwrite($fichierDataExodusTxt,"titreConference|".$titreConference."|
dateConference|".$dateConference."|
nomEnseignant|".$nomEnseignant."|
prenomEnseignant|".$prenomEnseignant."|
service|".$service."|
employeur|".$employeur."|
priseSon|".$priseSon."|
realisation|".$realisation."|
dureePresentation|".$dureePresentation."|
nombreDeDiapo|".$nombreDeDiapo."|
nombreDePartie|".$nombreDePartie."|
|
".$blocTDMData."|
".$blocMarqueursData."|");

	fclose($fichierDataExodusTxt);

?>