<?php

	/*--------------------------------------------------------------------------------------*\
	
		Pour écrire le fichier 'timesheet.smil', le programme a besoin de lire un fichier
	marqueur, comme on supprime les fichiers du serveur une fois utilisé et qu'il est possible
	de changer la valeur des marqueurs, si l'utilisateur n'a pas upload de fichier marqueur en 
	mode Correction, on doit en créer un. On récupère toutes les valeurs de la section 'Marqueur',
	et on les convertit en secondes pour créer le fichier marqueur.
	
	\*--------------------------------------------------------------------------------------*/


	//Initialise la variable
	$blocFichierMarqueursTxt = "";
	
	//Tant que 'nomMarqueurX' est déclaré on écrit le fichier marqueur
	for($i = 1; isset($_POST['nomMarqueur'.$i.'']); $i++){
		//Scinde dans un tableau les valeurs séparés par des ':'. ex : [0]=>'12',[1]=>'30',[2]=>'21' = '12:30:21'
		$temps = explode(':',$_POST['nomMarqueur'.$i.'']);

		//Comme dans l'écriture du timesheet.smil on lit le fichier des marqueurs et qu'on récupère des valeurs en secondes
		//on modifie le fichier marqueur directement pour ne pas rajouter des conditions et du traitement en plus,
		//et c'est pour ça qu'on convertit les valeurs du formulaire qui sont sous la forme 'HH:MM:SS' en secondes, 
		//pour que dans la suite du programme on puisse récupérer les modifications de l'utilisateur sans difficultés
		$seconde = $temps[0] * 3600;
		$seconde = $seconde + $temps[1] * 60;
		$seconde = $seconde + $temps[2];

		$blocFichierMarqueursTxt .= $seconde."	".PHP_EOL;
	}
	
	$fichierMarqueurTxt = fopen('dossierUt/'.$nomDossierUt.'/marqueur.txt','w');
	fwrite($fichierMarqueurTxt, $blocFichierMarqueursTxt);
	fclose($fichierMarqueurTxt);
	
	//Stock les informations du fichier 'marqueur' dans le tableau 'stockInfoFichierServeur'
	$stockInfoFichierServeur['fichierMarqueur'] = array('chemin'=> 'dossierUt/'.$nomDossierUt.'/marqueur.txt' , 'extension'=> 'txt', 'nom'=> 'marqueur', 'upload'=> FALSE);
?>